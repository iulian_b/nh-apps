<?php
include_once('init.php');

if (!isset($_SESSION['User'])) {
	header("Location: /login/index.php");
}

$user = new User();
$event = new Event();
$conference = new Conference();

$users_events = $user->getUsersEvents();
$events_users = $event->getEventsUsers();
$unsorted_users = $user->getAllUsers();
$sorted_users = $user->sortUsers($unsorted_users);
$x_events = $event->getDBEventsByName('X');
$conferences = $conference->getDBConferencesByName('carierainctrl');
$conf_speakers = $conference->getSpeakers();
$conf_topics = $conference->getTopics();

?>

<html>
	<head>
		<?php include_once('views/elements/header.html'); ?>
		<link href="resources/css/login.css" rel="stylesheet">
	</head>
	<body>
		<div class="container-fluid">
			<?php include_once('views/dashboard.php'); ?>
		</div>
	</body>
</html>