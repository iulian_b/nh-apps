<?php

include_once('init.php');
if (isset($_SESSION['User'])) {
	header("Location: /login/dashboard.php");
}
$user = new User();
if (isset($_POST['submit'])) {
	if ($_POST['submit'] == 'login') {
		$username = $_POST['email'];
		$password = $_POST['password'];

		$user->login($username, $password);
	} else if ($_POST['submit'] == 'logout') {
		$user->logout();
	}
}
?>
<html>
	<head>
		<?php include_once('views/elements/header.html'); ?>
		<link href="resources/css/login.css" rel="stylesheet">
	</head>
	<body>
		<div class="container">
		<?php
			if ($msg = $user->msg->getMessage()) {
				$user->msg->displayError($msg);
			}
		?>
		<?php include_once('views/login.html'); ?>
		</div>
	</body>
</html>