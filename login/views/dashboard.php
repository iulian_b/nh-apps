<nav class="navbar navbar-default" role="navigation">
	<div class="row">
		<div class="col-xs-6">
			<div class="navbar-text">Welcome <?=$_SESSION['User']['username'];?></div>
		</div>
		<div class="col-xs-6">
			<form class="navbar-text pull-right" action="index.php" method="post">
				<button type="submit" name="submit" value="logout">Logout</button>
			</form>
		</div>
	</div>
</nav>

<div id="accordion">
	<div class="row">
		<div class="col-sm-12 col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">
						<a data-toggle="collapse" data-target="#collapseLeftJoin" href="#collapseLeftJoin" class="collapsed">
							LEFT JOIN - All users
						</a>
					</h3>
				</div>
				<div id="collapseLeftJoin" class="panel-collapse collapse">
					<div class="panel-body">
					<?php foreach ($users_events as $user_event) { ?>
						<div class="list-group">
							<a href="#" class="list-group-item">
								<h4 class="list-group-item-heading"><?=$user_event['username'];?></h4>
								<p class="list-group-item-text">Event assigned: <?=$user_event['name'];?></p>
							</a>
						</div>
					<?php } ?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-12 col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">
						<a data-toggle="collapse" data-target="#collapseRightJoin" href="#collapseRightJoin" class="collapsed">
							RIGHT JOIN - All Events
						</a>
					</h3>
				</div>
				<div id="collapseRightJoin" class="panel-collapse collapse">
					<div class="panel-body">
					<?php foreach ($events_users as $event_users) { ?>
						<div class="list-group">
							<a href="#" class="list-group-item">
								<h4 class="list-group-item-heading"><?=$event_users['name'];?></h4>
								<p class="list-group-item-text">Event assigned: <?=$event_users['username'];?></p>
							</a>
						</div>
					<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12 col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">
						<a data-toggle="collapse" data-target="#collapseUnsorted" href="#collapseUnsorted" class="collapsed">
							Unsorted Users
						</a>
					</h3>
				</div>
				<div id="collapseUnsorted" class="panel-collapse collapse">
					<div class="panel-body">
					<?php foreach ($unsorted_users as $user) { ?>
						<div class="list-group">
							<a href="#" class="list-group-item">
								<h4 class="list-group-item-heading"><?=$user['username'];?></h4>
								<p class="list-group-item-text">Varsta: <?=$user['age'];?></p>
								<p class="list-group-item-text">Oras: <?=$user['city'];?></p>
							</a>
						</div>
					<?php } ?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-12 col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">
						<a data-toggle="collapse" data-target="#collapseSorted" href="#collapseSorted" class="collapsed">
							Sorted Users
						</a>
					</h3>
				</div>
				<div id="collapseSorted" class="panel-collapse collapse">
					<div class="panel-body">
					<?php foreach ($sorted_users as $user) { ?>
						<div class="list-group">
							<a href="#" class="list-group-item">
								<h4 class="list-group-item-heading"><?=$user['username'];?></h4>
								<p class="list-group-item-text">Varsta: <?=$user['age'];?></p>
								<p class="list-group-item-text">Oras: <?=$user['city'];?></p>
							</a>
						</div>
					<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12 col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">
						<a data-toggle="collapse" data-target="#collapseEvents" href="#collapseEvents" class="collapsed">
							Class Inheritence &gt; Get X Events
						</a>
					</h3>
				</div>
				<div id="collapseEvents" class="panel-collapse collapse">
					<div class="panel-body">
						<?php foreach ($x_events as $event) { ?>
						<div class="list-group">
							<a href="#" class="list-group-item">
								<h4 class="list-group-item-heading"><?=$event['name'];?></h4>
								<?php foreach ($event as $key => $value) { ?>
									<p class="list-group-item-text"><?=$key;?> - <?=$value;?></p>
								<?php } ?>
							</a>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>

		<div class="col-sm-12 col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">
						<a data-toggle="collapse" data-target="#collapseConference" href="#collapseConference" class="collapsed">
							Class Inheritence &gt; Get Conference carierainctrl
						</a>
					</h3>
				</div>
				<div id="collapseConference" class="panel-collapse collapse">
					<div class="panel-body">
						<?php foreach ($conferences as $conf) { ?>
						<div class="list-group">
							<a href="#" class="list-group-item">
								<h4 class="list-group-item-heading"><?=$conf['name'];?></h4>
								<p class="list-group-item-text">Speakers - 
								<?php foreach($conf_speakers as $speaker) { ?>
									<?=$speaker;?>,
								<?php } ?>
								</p>
								<p class="list-group-item-text">Topics - 
								<?php foreach($conf_topics as $topic) { ?>
									<?=$topic;?>,
								<?php } ?>
								</p>
								<br>
								<?php foreach ($conf as $key => $value) { ?>
									<p class="list-group-item-text"><?=$key;?> - <?=$value;?></p>
								<?php } ?>
							</a>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>