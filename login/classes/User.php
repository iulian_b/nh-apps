<?php

Class User {
	protected $_db = null;
	public $msg = null;

	public function __construct() {
		$this->_db = DbConn::getInstance();
		$this->msg = new UserMessage();
	}

	/**
	 *
	 * Exemplu pentru Login
	 *
	 */
	
	public function login($email, $password) {
		if (!empty($email) || !empty($password)) {
			$password = md5($password);
			$stmt = $this->_db->prepare("SELECT * FROM users WHERE email = :email AND password = :password");
			$stmt->bindParam(':email', $email);
			$stmt->bindParam(':password', $password);
			$stmt->execute();

			if ($stmt->rowCount()) {
				$user_row = $stmt->fetch(PDO::FETCH_ASSOC);
				$_SESSION['User'] = $user_row;
				header("Location: /login/dashboard.php");
			} else {
				$this->msg->setMessage("Incorrect Email or Password");
			}
		} else {
			$this->msg->setMessage("You need to entry an email and a password");
		}
	}

	public function logout() {
		session_destroy();
	}

	/**
	 *
	 * Exemplu pentru LEFT JOIN
	 *
	 */
	public function getUsersEvents() {
		$stmt = $this->_db->prepare("SELECT users.username, events.name 
			FROM users LEFT JOIN events ON users.id = events.user_id");
		$stmt->execute();
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}

	/**
	 *
	 * Exemplu pentru sortare
	 *
	 */
	public function getAllUsers() {
		$stmt = $this->_db->prepare("SELECT * FROM users");
		$stmt->execute();
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}
	public function sortUsers($unsorted_users) {
		usort($unsorted_users, function($a, $b) {
			return $b['age'] - $a['age'];
		});
		return $unsorted_users;
	}
}