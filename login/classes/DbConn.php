<?php

class DbConn extends PDO{
	private static $_instance = null;

	public function __construct($dbhost, $dbname, $dbuser = 'root', $dbpass = '', $dbtype = 'mysql') {
		parent::__construct($dbtype.':host='.$dbhost.';dbname='.$dbname, $dbuser, $dbpass);
	}

	public static function getInstance() {
		if (!isset(self::$_instance)) {
			try {
				self::$_instance = new DbConn("localhost", "nh-apps", "outsider", "outsider_123", "mysql");
			} catch (PDOException $e) {
				die($e->getMessage());
			}
		}

		return self::$_instance;
	}
}