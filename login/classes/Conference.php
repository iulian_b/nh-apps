<?php

Class Conference extends Event {
	protected $_speakers = ['Ion', 'Petru', 'Gigi'];
	protected $_topics = ['Despre Una', 'Despre Alta'];
	private $_conference;

	public function getDBConferencesByName($name) {
		$stmt = $this->_db->prepare("SELECT * FROM events 
									WHERE event_type='conference'
									AND name=:name");
		$stmt->bindParam(':name', $name);
		$stmt->execute();
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}

	public function getSpeakers() {
		return $this->_speakers;
	}

	public function setSpeakers($_speakers) {
		$this->_speakers = $_speakers;
	}

	public function getTopics() {
		return $this->_topics;
	}

	public function setTopics($_topics) {
		$this->_topics = $_topics;
	}

	public function getConference() {
		return $this->_conference;
	}

	public function setConference($_conference) {
		$this->_conference = $_conference;
	}
}