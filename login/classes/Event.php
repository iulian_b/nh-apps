<?php

Class Event {
	protected $_db = null;
	protected $_name;

	public function __construct() {
		$this->_db = DbConn::getInstance();
	}

	/**
	 *
	 * Exemplu pentru RIGHT JOIN
	 *
	 */
	public function getEventsUsers() {
		$stmt = $this->_db->prepare("SELECT users.username, events.name 
			FROM users RIGHT JOIN events ON users.id = events.user_id");
		$stmt->execute();
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}

	public function getDBEventsByName($name) {
		$stmt = $this->_db->prepare("SELECT * FROM events WHERE name=:name");
		$stmt->bindParam(':name', $name);
		$stmt->execute();
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}

	public function getName() {
		return $this->_name;
	}

	public function setName($name) {
		$this->_name = $name;
	}
}