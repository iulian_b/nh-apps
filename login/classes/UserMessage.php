<?php

Class UserMessage {
	private static $_errTemplate = 'error';
	private $_msg = null;

	public function displayError($msg) {
		include_once("views/elements/".self::$_errTemplate.".php");
	}

	public function setMessage($msg) {
		$this->_msg = $msg;
	}

	public function getMessage() {
		return $this->_msg;
	}
}